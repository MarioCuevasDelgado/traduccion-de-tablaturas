#defmodule Tablature do
  # def parse(tab) do
  #   tab
  #   |> String.split()
  #   |> Enum.map(fn line -> parse_line(line) end)
  #   |> Enum.filter(fn lista -> lista !=[] end)
  #   |> Enum.zip
  #   |> Enum.map(fn t-> Tuple.to_list(t) |> Enum.join(" ")end)
  #   |> Enum.join(" ")
  # end

  # def parse_line(line) do
  #   Regex.scan(~r/\d+/, line)
  #    |> List.flatten
  #    |> Enum.map(fn e ->String.at(line,0) <> e end)
  #   # |> Enum.join(" ")
  # end

  #=====================================================================================================================================================================

  # def parse(tab) do
  #   tab
  #   |> String.split()
  #   |> Enum.map(fn line -> parse_line(line) end)
  #   |> Enum.filter(fn lista -> lista != [] end)
  #   |> List.flatten()
  #   |> Enum.sort_by(fn {_a, b} -> b end, :asc)
  #   |> Enum.group_by(fn {_a, b} -> b end)
  #   |> Enum.map(fn {_a, b} -> b end)
  #   |> Enum.map(fn x -> if(length(x) > 1) do Kernel.elem(hd(x), 0) <> "/" <>
  #     Kernel.elem(hd(tl(x)), 0) else  Kernel.elem(hd(x), 0) end end)
  #   #|>Enum.map(fn x -> List.to_tuple(x)end)
  #   #|> Enum.map(fn {_a,b} -> Enum.join(b,"/") end)
  #   #|> Enum.map(fn {a, _b} -> a end)
  #   |> Enum.join(" ")
  # end

  # def parse_line(line) do
  #   line
  #   |> String.codepoints
  #   |> Enum.with_index
  #   |> Enum.filter(fn {a, _b} -> a != "-" and a != "|" end)
  #   |> tl
  #   |> Enum.map(fn {a, b} -> {String.at(line, 0) <> a, b} end)
  # end

#==============================================================================================

#end
defmodule Tablature do


  def borrar(tab) do
    if(length(tab) > 1) do
      Enum.reject(tab, fn {a, b} -> if(a == "_") do {a, b} end end)
    else tab
    end
  end

  def parse(tab) do
    tab
    |> String.split()
    |> Enum.map(fn line -> parse_line(line) end)
    |> Enum.filter(fn lista -> lista != [] end)
    |> List.flatten()
    |> Enum.sort_by(fn {_a, b} -> b end, :asc)
    |> Enum.group_by(fn {_a, b} -> b end)
    |> Enum.map(fn {_a, b} -> b end)
    |> Enum.map(fn e -> e
    |> Enum.uniq end)
    |> Enum.map(fn e -> e
    |> Enum.sort_by(fn {a, _b} -> String.length(a) end, :desc) end)
    |> Enum.map(fn e -> e
    |> borrar end)
    |> Enum.map(fn e -> if(length(e) > 1) do tl(e)
    |> Enum.reduce(elem(hd(e), 0), fn f, acc -> acc <> "/" <> Kernel.elem(f, 0) end) else Kernel.elem(hd(e), 0) end end)
    |> Enum.join(" ")
  end


  def parse_line(line) do
    line
    |> String.codepoints
    |> Enum.with_index
    |> Enum.map(fn {x, y} -> if(rem(y, 2) == 1 and x == "-") do {"_", y} else {x, y} end end)
    |> Enum.filter(fn {a, _b} -> a != "-" and a != "|" end)
    |> tl
    |> Enum.map(fn {a, b} -> if(a == "_") do {a, b} else {String.at(line, 0) <> a, b} end end)
  end
end
